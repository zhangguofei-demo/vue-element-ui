import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'index',
        component: () => import('@/views/index.vue')
    },
    {
        path: '/type',
        name: 'list',
        component: () => import('@/views/type/list.vue')
    },
    {
        path: '/app',
        name: 'list',
        component: () => import('@/views/app/list.vue')
    },
    {
        path: '/equip',
        name: 'list',
        component: () => import('@/views/equip/list.vue')
    },
    {
        path: '/manufacturer',
        name: 'list',
        component: () => import('@/views/manufacturer/list.vue')
    }
]

const router = new VueRouter({
  routes,
  mode:"history"
})

export default router
