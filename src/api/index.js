import axios from "axios";
import { Message } from "element-ui";
import router from "@/router";
// 创建axios实例
const service = axios.create({
    // api 的 base_url
    baseURL: "http://127.0.0.1",
    timeout: 15000 // 请求超时时间
});

// request 请求拦截器
service.interceptors.request.use(
    config => {

        //设置header
        config.headers["Content-Type"] = "application/x-www-form-urlencoded";
        config.headers["Content-Type"] = "application/json;  charset=UTF-8";

        // 让每个请求携带自定义token 请根据实际情况自行修改
        if (sessionStorage.getItem("token")) {
            // header添加token
            config.headers["Authorization"] = sessionStorage.getItem("token");
        }
        // console.log("请求前拦截---------",config)
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);
// respone 响应拦截器
service.interceptors.response.use(
    response => {

        // console.log("请求之后拦截器1===========",response)
        if(response.data.code == 400){
            Message.error({
                message: response.data.msg,
                center: true,
                type: 'error',
                duration: 1000
            });

        }
        return response;
    },
    error => {
        console.log("请求之后拦截器2===========",error)
        // 错误信息提示，具体配置自己修改
        if (error.response.status == 400) {
            Message({
                message: "参数信息有误",
                center: true
            });
            return;
        } else if (error.response.status == 401) {
            Message({
                message: "请重新登陆",
                center: true
            });
            router.push("/login");
            return;
        } else if (error.response.status == 404) {
            Message({
                message: "用户不存在",
                center: true
            });
            return;
        } else if (error.response.status == 500) {
            Message({
                message: "服务器内部错误",
                center: true
            });
            return;
        } else if (error.response.status == 560) {
            Message({
                message: "数据库异常",
                center: true
            });
            return;
        }
        console.log("err" + error); // for debug
        Message({
            message: error.message,
            type: "error",
            duration: 5 * 1000
        });
        return Promise.reject(error);
    }
);

export default service;
