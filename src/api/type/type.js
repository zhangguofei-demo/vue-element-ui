import axios from '../index'

// 查询一级分类
export function getList (params) {
    return axios({
        method: "post",
        url: "/api/type/list",
        params: params
    })
}
//根据父级id查询分类
export function byParentId (params) {
    return axios({
        method: "post",
        url: "/api/type/by-parentId",
        params: params
    })
}
// 级联：查询一级分类
export function cascaderGetList (params) {
    return axios({
        method: "post",
        url: "/api/type/cascader-list",
        params: params
    })
}
//级联：根据父级id查询分类
export function cascaderByParentId (params) {
    return axios({
        method: "post",
        url: "/api/type/cascader-by-parentId",
        params: params
    })
}
//级联：获取父级ids
export function getParentIds (params) {
    return axios({
        method: "post",
        url: "/api/type/get-parentIds",
        params: params
    })
}
//添加分类
export function save (params) {
    return axios({
        method: "post",
        url: "/api/type/save",
        params: params
    })
}

//修改分类
export function update (params) {
    return axios({
        method: "post",
        url: "/api/type/update",
        params: params
    })
}

//删除分类
export function del (params) {
    return axios({
        method: "post",
        url: "/api/type/del",
        params: params
    })
}

//获取分类列表
export function getType (params) {
    return axios({
        method: "post",
        url: "/api/type/get-type",
        params: params
    })
}


