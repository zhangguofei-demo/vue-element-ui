import axios from '../index'

// 分页
export function getList (params) {
    return axios({
        method: "post",
        url: "/api/app/list",
        params: params
    })
}

//查询分类
export function getType (params) {
    return axios({
        method: "post",
        url: "/api/app/get-type",
        params: params
    })
}

//查询厂商
export function getManufacturer (params) {
    return axios({
        method: "post",
        url: "/api/app/get-manufacturer",
        params: params
    })
}



//添加分类
export function save (params) {
    return axios({
        method: "post",
        url: "/api/app/save",
        params: params
    })
}

//修改分类
export function update (params) {
    return axios({
        method: "post",
        url: "/api/app/update",
        params: params
    })
}

//删除分类
export function del (params) {
    return axios({
        method: "post",
        url: "/api/app/del",
        params: params
    })
}

