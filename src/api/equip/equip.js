import axios from '../index'

// 分页
export function getList (params) {
    return axios({
        method: "post",
        url: "/api/equip/list",
        params: params
    })
}




//添加分类
export function save (params) {
    return axios({
        method: "post",
        url: "/api/equip/save",
        params: params
    })
}

//修改分类
export function update (params) {
    return axios({
        method: "post",
        url: "/api/equip/update",
        params: params
    })
}

//删除分类
export function del (params) {
    return axios({
        method: "post",
        url: "/api/equip/del",
        params: params
    })
}

